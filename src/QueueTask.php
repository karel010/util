<?php namespace Ewise\Util;

/**
 * Base class for creating a task handler for E-wise Queue.
 * @abstract
 */
abstract class QueueTask
{
    abstract protected function handle($uuid, $data);
    const TASK_NAME = 'OVERRIDE_ME!';

    protected $dbh;

    /**
     * Create and clear items from the queue untill there are no more.
     * This includes items that have been added while the task has been running.
     */
    public function __construct(\PDO $dbh)
    {
        $this->dbh = $dbh;
        $dbh->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );

        do {
            $items = $this->fetchItems();
            foreach ($items as $qid => $data) {
                try {
                    $this->handle($qid, $data);
                } catch (\Exception $e) {
                    $message = $e->getMessage();
                    Log::error($message);
                    $sth = $dbh->prepare('UPDATE queue SET error = :message WHERE qid = :qid')->execute([
                        ':qid' => $qid,
                        ':message' => $message,
                    ]);
                    continue;
                }

                $this->handled($qid);
            }
        } while(!empty($items));
    }

    /**
     * Mark items in queue as being handled and fetch those items.
     *
     * @return array Decoded data from queue table keyd by qid.
     */
    protected function fetchItems()
    {

        $params = [
            ':task' => $this->taskName(),
            ':pid' => getmypid(),
        ];

        $this->dbh->beginTransaction();

        try {
            $sth = $this->dbh->prepare('UPDATE queue SET pid = :pid, error = NULL WHERE task = :task AND pid = 0');
            $sth->execute($params);

            $sth = $this->dbh->prepare('SELECT qid, data FROM queue WHERE task = :task AND pid = :pid AND error IS NULL ORDER BY qid');
            $sth->execute($params);

            $this->dbh->commit();
        } catch (\PDOException $e) {
            $this->dbh->rollBack();
            throw($e);
        }

        $items = $sth->fetchAll(\PDO::FETCH_KEY_PAIR);

        foreach ($items as $qid => &$data ) {
            $data = $this->decodeData($data);
        }

        return $items;
    }

    /**
     * Unserializes data from the data column in queue table.
     * Override this method when a diferent encoding is used.
     */
    protected function decodeData(string $encoded)
    {
        return unserialize($encoded);
    }

    /**
     * Moves handled item to queue_history table.
     *
     * @param int $qid Id of the handled entry in queue.
     */
    protected function handled(int $qid)
    {
        $this->dbh->beginTransaction();
        $params = [':qid' => $qid];
        
        try {
            $sth = $this->dbh->prepare('INSERT INTO queue_history (qid, queued, task, gid, pid, data) SELECT qid, queued, task, gid, pid, data FROM queue WHERE qid = :qid');
            $sth->execute($params);

            $sth = $this->dbh->prepare('DELETE FROM queue WHERE qid = :qid');
            $result = $sth->execute($params);

            $this->dbh->commit();
        } catch (\PDOException $e) {
            $this->dbh->rollBack();
            throw($e);
        }
    }

    /**
     * TASK_NAME getter.
     * @return string Contents of the "overridden" TASK_NAME constant.
     */
    protected function taskName() {
        return get_called_class()::TASK_NAME;
    }
}
