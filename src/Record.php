<?php namespace Ewise\Util;

/**
 * Class Record
 *
 * Records are a smarter form of stdClass. Record users simply access properties e.g. $record->name.
 * Advantages over using stdClass:
 *  Attributes aliases; If system A calls something 'name' and system B call it 'FullName' both can use the same property with native naming.
 *  Default values; Because we explisitly decleare the properties we want we can also add default values
 *  Filtering; Only known properties are set rest is discarded. If needed discarding can be logged.
 *  Accessors; Conversions and transformations can be done by using accessors. When setAttribute() exists it will be used even when doing $record->property = value.
 */
abstract class Record
{
    private $__aliases = [];
    private $__id;
    
    /**
     * Add property aliases
     *
     * @param array $mapping Array keyd by alias and the real property name as value
     */
    public function addAliases(array $mapping) {
        $this->__aliases += $mapping;
    }

    /**
     * Add single property alias
     *
     * @param string $alias Extra name of the property.
     * @param string $property The name of the original property
     */
    public function addAlias($alias, $property) {
        $this->addAliases([$alias => $property]);
    }

    /**
     * Fill properties with data of an existing object or array.
     *
     * @param mixed $keyd_data An itterable with (some of the) keys matching attribute
     */
    public function addValues($keyd_data) {
        foreach ($keyd_data as $property => $value) {
            $this->__set($property, $value);
        }
    }
    
    public function &__get(string $name) {
        if (isset($this->__aliases[$name])) {
            $name = $this->__aliases[$name];
        }
        
        $getter = 'get' . ucfirst($name);
        if (is_callable([$this, $getter])) {
            $value = $this->{$getter}();
            return $value; // Only variables may be returned as reference
        }
        
        return $this->{$name};
    }
    
    public function __set(string $name, $value) {
        if (isset($this->__aliases[$name])) {
            $name = $this->__aliases[$name];
        }
        
        $setter = 'set' . ucfirst($name);
        if (is_callable([$this, $setter])) {
            return $this->{$setter}($value);
        }
        
        if (!property_exists($this, $name)) {
            Log::notice(static::class . " does not have \"$name\" property");
            return;
        }
        
        $this->{$name} = $value;
    }

    public function __isset(string $name) {
        return $this->__get($name) !== null;
    }
}
