<?php namespace Ewise\Util;

interface DataProvider extends \Iterator
{
    public function setRecordTemplate(Record $record);
    public function nextRecord();
    public function getRecords();
    public function updateRecord(Record $record);
}
