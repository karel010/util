<?php namespace Ewise\Util;

/**
 * Class Log
 *
 * Bunch of static methods to aid in repporting and debugging.
 */
class Log
{

    private static $level = E_USER_WARNING;
    private static $exitCode = 0;

    /**
     * Smart print function.
     * Don't worry about newlines. Add 'Myvar: ' by using $prefix. Print arrays and objects.
     *
     *  @param $mixed Subject that is to be printed.
     *  @param $prefix Optional, prepended to simple subjects before printing. Includes ':'.
     */
    public static function p($mixed, string $prefix = null) {
        if (is_string($mixed) || is_numeric($mixed)) {
            $prefix .= isset($prefix)? ": " : "";
            echo $prefix . $mixed . "\n";
        }
        else {
            print_r($mixed);
        }
    }

    /**
     * Log level accessor.
     *
     * @param $level When given the self::$level is set to this level valid values are:
     *      E_USER_NOTICE,
     *      E_USER_WARNING,
     *      E_USER_ERROR,
     * @return const The current loglevel
     */
    public static function level($level = null) {
        if (isset($level)) {
            self::$level = $level;
        }

        return self::$level;
    }

    public static function exitCode(int $exitCode = null) {
        if (isset($exitCode)) {
            self::$exitCode = $exitCode;
        }

        return self::$exitCode;
    }

    public static function notice($message) {
        if (self::$level === E_USER_NOTICE) {
            error_log(self::decorateMessage('Notice', $message));
        }
    }

    public static function warning($message) {
        if (self::$level !== E_USER_ERROR) {
            error_log(self::decorateMessage('Warning', $message));
        }
    }

    public static function error($message) {
        error_log(self::decorateMessage('Error', $message));
        if (self::$exitCode === 0) {
            self::$exitCode = 2;
        }
    }

    public static function fatal($message) {
        error_log(self::decorateMessage('Fatal Error', $message));
        debug_print_backtrace(0, 6);
        exit(1);
    }

    private function decorateMessage($level, $message) {
        $debug = debug_backtrace()[1];
        return sprintf("PHP $level: $message in %s on line %d", $debug['file'], $debug['line']);
    }
}
