<?php namespace Ewise\Util;

abstract class FetchallProvider implements DataProvider
{
    abstract public function updateRecord(Record $record);
    abstract protected function buildRaw();

    protected $position = 0;
    protected $first = true;
    protected $raw = [];
    protected $records = [];
    protected $template;
    protected $source;

    /**
     * Default implementation
     */
    public function __construct($source) {
        $this->source = $source;
        $buildOutput = $this->buildRaw();

        // buildRaw() can either set raw directly or output an array
        if (!empty($buildOutput)) {
            $this->raw = $buildOutput;
        }
    }

    public function setRecordTemplate(Record $template) {
        $this->template = $template;
    }

    /** Do we want to keep this, since we implement Iterator interface? **/
    public function nextRecord() {
        if (!$this->first) {
            $this->next();
        }

        if ($this->valid()) {
            $this->first = false;
            return $this->current();
        }
    }

    /*
     * Fetch all records
     */
    public function getRecords() {
        if (!empty($this->raw)) {
            reset($this->raw);
            $this->position = key($this->raw);

            while ($this->valid) {
                $this->current();
                $this->next();
            }
        }

        return $this->records;
    }

    public function rewind() {
        $this->position = 0;
        $this->first = true;
    }

    public function current() {
        if (!isset($this->records[$this->position])) {
            $rawValues = $this->raw[$this->position];
            $record = $this->createRecord($rawValues);
            $this->modifyRecord($record, $rawValues);

            $this->records[$this->position] = $record;
            unset($this->raw[$this->position]);
        }

        return $this->records[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        $this->first = false;
        ++$this->position;
    }

    public function valid() {
        return ( isset($this->raw[$this->position]) || isset($this->records[$this->position]));
    }
    
    /**
     * Create a new instance of the record based on the template
     */
    protected function createRecord($rawValues) {
        $record = clone $this->template;
        $record->addValues($rawValues);

        return $record;
    }
    
    /**
     * Hook for some post-processing that can not be done by the record itself.
     */
    protected function modifyRecord(Record $record, $rawValues) {}
}
