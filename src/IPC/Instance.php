<?php namespace Ewise\Util\IPC;

/**
 * An Instance is the client side of the comunication.
 * Because it executes the IPC wrapper it is expected to live longer than the wrapper.
 *
 * On launch the wrapper script is given a unique path to a socket that it suposed to create.
 */
class Instance 
{
    private $socket;
    
    public function comunicate($data) 
    {
        if (fwrite($this->socket, $data) === FALSE) {
            throw new \Exception("Could not write to CasIPCWrapper.");
        }

        $message = fgets($this->socket);
        if (($message === FALSE) && !feof($this->socket)) {
            throw new \Exception("Could not read from CasIPCWrapper.");
        }

        return $message;
    }
    
    public function __construct(string $wrapper, string $log = '/dev/null', string $socket_path = '/tmp')
    {
        if (is_dir($socket_path)) {
            $socket_path .= '/phpipc_'. getmypid() .'.sock';
        }

        //   PerlIPCWrapper.pl socket output redirect background
        $command = sprintf('%s %s     >%s    2>&1     &', $wrapper, $socket_path, $log);
        system($command);
        
        for ($backoff = 300000, $max = 9000000;!file_exists($socket_path) && $backoff <= $max; $backoff += $backoff) {
            usleep($backoff);            
        }
        
        $this->socket = stream_socket_client("unix://$socket_path", $errno, $errstr);
        if ($this->socket === FALSE) {
            echo file_get_contents($log);
            throw new \Exception("Could not connect to IPCWrapper.");
        }
        
        stream_set_timeout($this->socket, 900); // Support long running commands
    }    
    
    public function __destruct() 
    {
        fclose($this->socket);
    }
}
