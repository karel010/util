<?php namespace Ewise\Util\IPC;


use Ewise\Util\Result;
use Ewise\Util\ResultInstance;

class Receiver
{
    private $target;
    private $instance;

    public function __construct($target, Instance $instance)
    {
        $this->target = $target;
        $this->instance = $instance;
    }

    public function run($data = NULL)
    {
        $result = new ResultInstance($data);

        while(TRUE) {
            $command = $this->communicate($result);

            if ($command === NULL) {
                throw new \Exception('Connection closed unexpectedly');
            }

            if (empty($command->method)) {
                throw new \Exception('No method provided');
            }

            if ($command->method === '_FINISHED_') {
                return;
            }

            $result = $this->proxy($command);
        }
    }

    protected function communicate(Result $result)
    {
        $data = (object) [
          'result' => $result->result(),
          'error' => $result->error(),
        ];

        $json = $this->instance->comunicate(json_encode($data) . "\n");
        return json_decode($json);
    }


    protected function proxy($command) : Result
    {
        if (!method_exists($this->target, $command->method)) {
            throw new \Exception('Method: "' . $command->method . '" not present in $target');
        }

        $result = $this->target->{$command->method}(...$command->arguments);
        return new ResultInstance($result);
    }
}