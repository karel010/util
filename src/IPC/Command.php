<?php namespace Ewise\Util\IPC;

use Ewise\Util\Result;

class Command implements Result
{
    private $result;
    private $error;
    private $ipc;
    private $command;
    
    public function __construct(Instance $ipc, string $command) 
    {
        $this->ipc = $ipc;
        $this->command = $command;
    }
    
    public function execute(array $parameters = []) 
    {
        $request = new \stdClass;
        $request->command = $this->command;
        $request->parameters = $parameters;
        
        $json = json_encode($request);
        $this->result = $this->comunicate($json);
        
        return $this;
    }
    
    public function result() 
    {
        return $this->result;
    }
    
    public function error() 
    {
        return $this->error;
    }    
    
    private function comunicate($json) 
    {
        $result = $this->ipc->comunicate($json . "\n");
        $result = json_decode($result);
        
        if(isset($result->error)) {
            $this->error = $result->error;
            return NULL;
        }//else
        
        $this->error = NULL;        
        return $result;
    }
}
