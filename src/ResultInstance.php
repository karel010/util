<?php namespace Ewise\Util;


class ResultInstance implements Result
{
    public $result;
    public $error = NULL;

    public function __construct($resultData, $error = NULL)
    {
        $this->result = $resultData;
        $this->error = $error;

        // Result interface implemented
        if ($resultData instanceof Result) {
            $this->result = $resultData->result();
            $this->error = $resultData->error();
            return;
        }

        // Result structured array
        if (is_array($resultData)) {
            if (array_key_exists('result', $resultData) && array_key_exists('error', $resultData)) {
                $this->result = $resultData['result'];
                $this->error = $resultData['error'];
                return;
            }
        }

        // Result structured object
        if (is_object($resultData)) {
            if(property_exists( $resultData, 'result') && property_exists($resultData, 'error')) {
                $this->result = $resultData->result;
                $this->error = $resultData->error;
                return;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function result()
    {
        return $this->error ? NULL : $this->result;
    }

    /**
     * @inheritDoc
     */
    public function error()
    {
        return $this->error;
    }
}