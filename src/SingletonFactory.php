<?php namespace Ewise\Util;

/**
 * Creates a singleton of existing classes
 *
 * @author karel
 */
class SingletonFactory 
{
  private static $objects = array();
  
  static function Instance($classname) {
    if (isset(self::$objects[$classname])) {
      return self::$objects[$classname];
    }
    
    $args = func_get_args();
    array_shift($args);
    
    $reflect = new \ReflectionClass($classname);
    self::$objects[$classname] = $reflect->newInstanceArgs($args);
    
    return self::$objects[$classname];
  }
  
}