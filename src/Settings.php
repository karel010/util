<?php namespace Ewise\Util;

class Settings
{
    const FILE_NAME = 'settings.json';
    const VIRTUAL = '/dev/null';
    const DB_HOST = 'aurora.e-wise.com';
    const E_NOT_FOUND = 404;

    private $path = self::VIRTUAL; // Always overwriten in default construtor.
    private $settings;

    /**
     * Load settings from json file.
     *
     * @param string $path Path to settings file. Will be resolved if NULL
     * @param bool $create Flag for creating a new configuration. Skips reading from path.
     * @throws \Exception
     */
    public function __construct(string $path = NULL, bool $create = FALSE)
    {
        if ($path === NULL) {
           $path = $this->resolvePath();
        }

        $this->path($path);

        if ($create || $path === self::VIRTUAL) {
            $this->raw(new \stdClass);
        } else {
            $json = file_get_contents($path);
            $this->raw($this->decode($json));
        }
    }

    /**
     * Get a setting
     *
     * @return mixed Returns the value or NULL if the setting is not present.
     */
    public function get(string $setting)
    {
        return $this->settings->$setting ?? NULL;
    }


    /**
     * Set a setting
     */
    public function set(string $setting, $value)
    {
        $this->settings->$setting = $value;
    }

    /**
     * Get or set the entire settings opject
     *
     * @param stdClass $raw A serializable configuration.
     * @return stdClass The internal $settings object.
     */
    public function raw($raw = NULL)
    {
        if (isset($raw)) {
            $this->settings = $raw;
        }

        return $this->settings;
    }

    /**
     * Get or set the path to the settings file
     *
     * @param string $path Path to settings file.
     * @return string Path to the settigns file.
     */
    public function path(string $path = NULL) : string
    {
        if (isset($path)) {
            $this->path = $path;
        }

        return $this->path;
    }

    /**
     * Write the configuration to file.
     */
    public function save()
    {
        file_put_contents($this->path(), json_encode($this->raw(), JSON_PRETTY_PRINT));
    }

    /**
     * Create database connection from settings.
     *
     * @return \PDO Database handle
     * @throws \Exception
     */
    public function database($database = NULL) : \PDO
    {
        $config = $this->get('database');

        // Test for mismatch in default database configuration.
        if (isset($database, $config->database) && $config->database != $database) {
            $config = NULL;
        }

        if (isset($this->raw()->databases->$database)) {
            $config = $this->raw()->databases->$database;
            $config->database = $config->database ?? $database;
        }

        if (empty($config)) {
            throw new \Exception(
                $database ? "Configuration for $database does not exist" : "No database configration provided"
            );
        }
        
        $host = $config->host ?? self::DB_HOST;
        $dsn = $config->dsn ?? "mysql:host=$host;dbname=$config->database";
        $options = $this->resolvePDOConstants($config->options ?? []);
        // Throw exceptions by default
        $options += [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION];

        return new \PDO($dsn, $config->username, $config->password, $options);
    }


    /**
     * JSON decode function with propper error checking.
     * FIXME: There is no way to set other decode options.
     *
     * @param string $json A string containing valid json.
     * @return mixed $decoded The decoded result.
     * @throws \Exception
     */
    static public function decode($json)
    {
        $decoded = json_decode($json);

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
            break;
            case JSON_ERROR_DEPTH:
                 throw new \Exception('JSON Decode - Maximum stack depth exceeded');
            break;
            case JSON_ERROR_STATE_MISMATCH:
                throw new \Exception('JSON Decode - Underflow or the modes mismatch');
            break;
            case JSON_ERROR_CTRL_CHAR:
                throw new \Exception('JSON Decode - Unexpected control character found');
            break;
            case JSON_ERROR_SYNTAX:
                throw new \Exception('JSON Decode - Syntax error, malformed JSON');
            break;
            case JSON_ERROR_UTF8:
                throw new \Exception('JSON Decode - Malformed UTF-8 characters, possibly incorrectly encoded');
            break;
            default:
                throw new \Exception('JSON Decode - Unknown error');
            break;
        }

        return $decoded;
    }

    /**
     * Locate the settings file using the likely locations.
     *
     * @return string Returns the path to an existing settings file or throws an exception.
     * @throws \Exception
     */
    static public function resolvePath($fileName = self::FILE_NAME) : string
    {
        // CWD = project dir        
        if (file_exists($fileName)) {
            return $fileName;
        }
        
        // CWD = project dir/bin
        if (file_exists('../' . $fileName)) {
            return '../' . $fileName;
        }
        
        // This file is in vendor/ewise/util/src
        if (file_exists(__DIR__ .'/../../../../' . $fileName)) {
            return __DIR__ .'/../../../../' . $fileName;
        }
        
        // This file is in project dir/src
        if (file_exists(__DIR__ .'/../' . $fileName)) {
            return __DIR__ .'/../' . $fileName;
        }

        throw new \Exception('Could not find ' . $fileName, self::E_NOT_FOUND);
    }

    /**
     * Resolves PDO constants in text to actual constants
     *
     * @param $options \Iterator Valid options list
     * @return array Options list with resolved keys and values
     * @throws \Exception If file path cannot be resolved
     */
    protected function resolvePDOConstants($options) : array
    {
        $resolved = [];

        foreach ($options as $option => $value) {
            if (is_string($option)) {
                if ($this->isPDOConstant($option)) {
                    $option = constant('\\' . $option);
                }
            }

            if (is_string($value)) {
                if ($this->isPDOConstant($value)) {
                    $value = constant('\\' . $value);
                }
            }

            // Fix paths relative to config file
            if (in_array($option, [\PDO::MYSQL_ATTR_SSL_CERT, \PDO::MYSQL_ATTR_SSL_KEY, \PDO::MYSQL_ATTR_SSL_CA])) {
                $value = self::resolvePath($value);
            }

            $resolved[$option] = $value;
        }

        return $resolved;
    }

    /**
     * Test for PDO constant in string
     *
     * @param string $string
     * @return bool
     */
    protected function isPDOConstant(string $string) : bool
    {
        return preg_match('/^\\\?PDO::/', $string) ? true : false;
    }
}
