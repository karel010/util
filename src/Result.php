<?php namespace Ewise\Util;

/**
 * Simpel result interface
 */
interface Result
{
    /**
     * Get the result.
     *
     * SHOULD return NULL when an error occurs else the application specific result.
     * 
     * @return mixed The result.
     */
    public function result();
    
    /**
     * Retreive an error when applicable.
     *
     * This method MUST always return NULL when no error occurd.
     * What is returned is application specific but MUST be consistent for that application.
     *
     * @return mixed The error.
     */
    public function error();
}
