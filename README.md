# E-wise Util

Goto library for every PHP commandline project.

## Index

1. [Settings handling](#markdown-header-1-settings-handling)
1. [Logging](#markdown-header-2-logging)
1. [Queue system](#markdown-header-3-queue-system)
1. [Systems integration framework](#markdown-header-4-systems-integration-framework)
    1. [Smart contract](#markdown-header-smart-contract)
    1. [Data iterator](#markdown-header-data-iterator)
    1. [Record consumer](#markdown-header-record-consumer)
1. [Singleton factory](#markdown-header-5-singleton-factory)
1. Inter proccess comunication

## 1. Settings handling

Most projects have some settings and or credentials.
These settings should go into a *settings.json* file that is not part of the repository.

### Loading and creating a settings file
The ```Ewise\Util\Settings``` class helps creating and loading json configuration files.

    <?php
        // Load settings from the default settings.json location.
        $settings = new Settings();

        // Load a specific file
        $composer = new Settings(__DIR__. "/../composer.json");

        // Create a new settings file
        $testConfig = new Settings('/tmp/my_test_file.json', true);
        $testConfig->set('helo' => 'Hello there!');
        $testconfig->save();

        // Create an in memory only settings file to pass on to objects for example
        $virtual = new Settings(Settings::VIRTUAL); // Saves to /dev/null

        // Getting a single setting (may be a complex value)
        $item = $settings->get('API_KEY');

        // Getting everything
        $everything = $settings->raw();

        // Raw can also be used to set a new configuration.
        $settings->raw($everything);

    ?>

### Database connections

Geting a database connection is something that is ofthen done and should be as simple as.

    <?php
        $settings = new Settings();
        $pdo = $settings->database();
    ?>

#### Default database configuration

For the statements above to work a specific configuration is required:

*NOTE: Comments are not allows in JSON but are added for clarification*

    {
        "database": {
            "database": "integration",
            "username": "SA",
            "password": "PASSWORD",
            "host": "production.example.com", // optional
            "dns": "sqlite:/opt/databases/mydb.sq3", // optional
            "options": {"PDO::MYSQL_ATTR_DIRECT_QUERY": true} // optional
        }
    }

#### Other databases

Aditional databases may be configured with the databases object by default the key is used as database name.

*Minimal configuration*

     <?php
        $namedDb = $settings->database('integration');
        $anotherDb =  $settings->database('mysql');
    ?>

    { // JSON
        "databases": {
            "integration": {
                "username": "SA",
                "password": "PASSWORD"
                // Same optional configuration as above may be used.
            },
            "mysql": {
                "username": "SA",
                "password": "PASSWORD"
            }
        }
    }


*Alternative name. The key does not have to match the db name*

    <?php
        if ($testing) {
            $pdo = $settings->database('dev-integration');
        }
    ?>

    { // JSON
        "databases": {
            "dev-integration" {
                "database": "integration", // Real database name used in connection
                "username": "SA",
                "password": "PASSWORD",
                "host": "devserver.example.com"
            }
        }
    }

#### TLS secured MySQL connections

Store certificates in a safe place and make sure they are not committed to the repository.

    {
        "database": {
            "database": "integration",
            "username": "SA",
            "password": "PASSWORD",
            "host": "production.example.com", // optional
            "options": {
                "PDO::MYSQL_ATTR_SSL_CERT": "keys/client-cert.pem",
                "PDO::MYSQL_ATTR_SSL_KEY": "keys/client-key.pem",
                "PDO::MYSQL_ATTR_SSL_CA": "keys/server-ca.pem",
                "PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT": false
            }
        }
    }

### Extras

Decoding json and handling errors properly is cumbersome in PHP.
Because the settings Class already needs to do this it sports a static decode method:

    <?php
        try {
            $obj = Settings::decode($json);
        } catch(\Exception $e) {
            // Something went wrong...
        }
    ?>

Often there are files that need to be resolved relative to the root of the project.
Because the settings Class already does this to locate the settings.json file it sport a static resolvePath method.
It is particulary handy when loading a specific json file:

    <?php
        try {
            $path = Settings::resolvePath('tests/databases.json'); // Never put credentials in a file other than settings.json
            $dbh = (new Settings($path))->database('test_sqlite_db');
        } catch (\Exception $e) {
            if (e->getCode() === Settings::E_NOT_FOUND) {
                // ResovlePath failed to locate the file.
            }
        }
    ?>

## 2. Logging

The ```Ewise\Util\Log``` class provides static methods to help with outputting information on STDOUT/STDERR.

*Printing stuff*

    <?php
        // Chuck anything in and it will print it with a newline adds anoptional prefix.
        // Complex variables will be printed with print_r
        $string = "print me"
        Log::p($string); // outputs: "print me\n"
        Log::p($string, "String"); // outputs: String: print me\n

        $array = ["print", "me"];
        Log::p($array); // equals: print_r($array);
    ?>

*Logging to stderr*

    <?php
        // Set the appropreate log level. Thes levels are predefined constants in php.
        // The default log level is E_USER_WARNING so notices are not displayd by default.
        // Setting log level to E_USER_ERROR also hides the warnings.
        Log::level(E_USER_NOTICE);

        // Log an unrecoverable error (logs error, a trace and exits with status 1)
        Log::fatal("Unrecoverable error occured");

        // Log an error. And continue.
        Log::error("An error occured");

        // Log a warning.
        Log::warning('This probably should not happen');

        // Log a debug statement.
        Log::notice("How dit this get executed?");
    ?>

*Exit status*

    <?php
        // Any program using the logging facility should exit using the exit code from Log.
        exit(Log::exitCode());

        Log::error(); // Exit code is 2
        Log::exitCode(3); // Exit code is 3
        Log::error(); // Exit code is still 3

    ?>


NOTE: Libraries SHOULD throw exceptions and not print things. The application SHOULD dictate what it outputs.

## 3. Queue system

This README only contains a quick usage excerpt.
The Queue system is explained in detail in a [google docs document](https://docs.google.com/document/d/1wWyhEQWx0lebEywCN--phxT1me-4BTqUW0K8BQI4lpk).

*File bin/mytask.sh*

    #!/usr/bin/env php
    <?php
        use Ewise\Util\Log;
        use Ewise\Util\Settings;
        use Ewise\Util\QueueTask;
        
        require __DIR__ . '/../vendor/autoload.php';

        /**
         * Put this in a seperate file when the class is not trivial
         */
        class MyTask extends QueueTask
        {
            const TASK_NAME = 'mytask';
            protected function handle($qid, $data)
            {
                if (empty($data)) {
                    throw new \Exception("We have no data. This is not correct");
                }

                // Process data
            }
        }

        $dhb = (new Settings)->database();
        new MyTask($dbh); // Runs in constructor

        exit(Log::exitCode());
    ?>

## 4. Systems integration framework

Systems itegration projects usually have the folowing pattern:

1. Fetch dataset(s) from source system (or initiating machine)
1. Transform the data into something the target system can use
1. Use the transformed data on the target system
1. (optional) Update the dataset(s) on the source system (mark as "in target system" for example).

The ```Record``` class and ```DataProvider``` interface (and it's base classes) help greatly with this pattern but are also useful components on there own.

### Smart contract

The Record class solves no. 2 in the pattern mentioned above. It also provides decoupling and a data definition.

Records are a basically smarter and more defined form of stdClass. Record users simply access properties e.g. $record->name like using stdClass.
Advantages over using stdClass:

 *  Attributes aliases - If system A calls something 'name' and system B call it 'FullName' both can use the same property with native naming.
 *  Default values - Because we explicitly declare the properties we want we can also add default values
 *  Filtering - Only known properties are set rest is discarded. If needed discarding can be logged.
 *  Accessors - Conversions and transformations can be done by using accessors. When setAttribute() exists it will be used even when doing $record->property = value.

Unless your data requirement is only an ID you are going to have to extend it. It is highly recommended that you model the record to the record consumer (target system).

*Extending the Record class*

    <?php
        class SyncRecord extends Record
        {
            // Define fields as protected attributes
            protected $initials;
            protected $surname;
            protected $email = "admin@example.com"; // Assign a default value
            protected $startDate;

            // Optionally add a constructor
            public function __construct($defaults = null)
            {
                if ($defaults !== null) {
                   $this->addValues($defaults);
                }
            }

            // Optionally define a setters and getters
            protected function setStartDate($value)
            {
                if (is_string($value)) {
                    $value = new \DateTime($value);
                    $value->setTimeZone(new \DateTimeZone('Europe/Amsterdam'));
                }

                $this->startDate = $value;
            }
            
            // A getter can define a virtual property.
            // The getter below for example creates $record->fullName.
            protected function getFullName()
            {
                return $this->initials . " " . "$this->surname";
            }

            /* Thats it nothing else should be added */
        }
    ?>


*Using the Record class*

    <?php
        $record = new SyncRecord();

        // Mapping foreign property names
        $record->addAlias('alias', 'propertyName'); // Map a single property

        // Mapping multiple aliases ['alias' => 'propertyName']
        $record->addAliases([
            'uid' => '__id', // $__id is the one property that exists in any record. I always use it as a source identifier.
            'mail' => 'email',
            'field_initials' => 'initials',
            'field_last_name_in_the_world' => 'surname',
        ]);


        // Set a bunch of values at once
        $record->addValues(['array' => 'or other itterable']);

        // Setting a single value
        $record->startDate = '@' . $timestamp;

        // Get data from a property
        $name = $record->fullName;
    ?>
    
### Data iterator

The DataProvider interface and the virtual class FetchAllProvider that implements it solve no. 1 and 4 in the pattern mentioned above.
A DataProvider implements the iterable interface. When iterating over the DataProvider Record objects are returned.

The returned Record object are clones of a record set as a template. As a result ```setRecordTemplate()``` MUST be called before the provider is used.
By setting a template record you determine what kind of record is returned and set default values, aliases for example.

*Creating a DataProvider that fetches all the data upfront in the constructor*

    <?php
        use Ewise\Util\FetchallProvider;
        use Ewise\Util\Record;

        class TaskProvider extends FetchallProvider
        {
            /**
             * Implements virtual FetchallProvider::buildRaw() called in constructor
             *
             * Use this method to fill $this->raw (directly or return an array)
             */
            protected function buildRaw()
            {
                // Return an array of iterables
                return $source->query("SELECT * FROM tasks")->fetchAll(\PDO::FETCH_ASSOC);
            }

            /**
             * Implements DataProvider::updateRecord()
             * Use this method to save back changes or mark the source data as handled.
             */
            protected function updateRecord(Record $record)
            {
                // Delete the handled task
                if ($record->handled) {
                    $sth = $pdo->prepare('DELETE FROM tasks WHERE tid = :tid');
                    $sth->execute([':tid' => $record->__id]);
                }
            }
        }
    ?>

*Using the DataProvider*

    #!/usr/bin/env php
    <?php
        use Ewise\Tasks;
        use Ewise\Util\Settings;
        use Ewise\Util\Log;

        // Set up the record template
        $template = new Tasks\TaskRecord; // Fictitious class
        $template->addAlias('tid', '__id');

        // Set up the DataProvider
        $dbh = (new Settings)->database();
        $provider = new Tasks\Provider($dbh);
        $provider->setRecordTemplate($template);

        // Set up the record handler/processor.
        $handler = new TaskHandler($dbh); // Fictitious class

        // Loop over the DataProvider to handle all records
        foreach ($provider as $record) {
            $handler->performTask($record);
            $provider->updateRecord($record);
        }
        
        exit(Log::exitCode());
        
        // NOTE: It is also possible to retrieve an array of all the records.
        $records = $provider->getRecords();
    ?>
    

### Record consumer

No. 3 in the patern mentioned above can be supported with an interface, but providing a general one in this toolbox is too limiting. For example one can no longer ask for a specific record type.
It would also remove the option to give some more meaningful name than *process* to the handling method (or methods).

*Example RecordProcessor interface*

    <?php
        interface SyncProcessor
        {
            /**
             * Process the record.
             * 
             * @return bool Indication of handled (TRUE) or skipped (FALSE) for the caller.
             */
            public function process(SyncRecord $record) :bool
        }

When the code calling the ```processor``` method needs to know whether followup actions need to be taken or not; it is a good idea to indicate with a boolean return.
If there is a need for complexer handling (e.g. different statuses or deeper code needs to know about the proccesing status) a ```success``` or ```error``` property in the ```Record``` should be used.

It is highly recommended that the ```processor``` method changes only the state of the provided ```$record``` but not of the object properties.
This guaranties that the handling of one record object does not influence another.

An interface only needs to be defined when there are more multiple processor classes that handle the same ```Record``` class.

## 5. Singleton factory

The singleton factory class can make a singleton out of any class. Do not that this can lead to side-effects and singletons should be avoided a much a possible.

*The usage the SingletonFactory class is simple but can be confusing.*

    <?php
        use Ewise\Util\SingletonFactory;

        // FIRST
        $dt = SingletonFactory::instance('\DateTime'); // Creates a DateTime object with the current time

        // Moving on
        sleep(5);
        $dt = SingletonFactory::instance('\DateTime'); // Returns the DateTime object created in FIRST

        // Constructor arguments can be passed as additional arguments to instance()
        $settings = SingletonFactory::instance(Settings::class, 'new_config.json', true);

        // Note that the arguments are NOT used as a key
        $dt = SingletonFactory::instance('\DateTime', 'yesterday'); // Still returns the DateTime object created in FIRST
    ?>